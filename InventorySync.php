<?php
ini_set('max_execution_time', 600); //300 seconds = 5 minutes
include_once('Functions/DBFunctions.php');
$conn = ConnectToVam();

include_once('Functions/MAGFunctions.php');
SoapConnect();

include_once('Functions/VAMFunctions.php');
include_once('Models/VAMSalesOrderClass.php');
include_once('Models/VAMInventoryClass.php');


/* POST processing*/

/** Prepare Inventory Export Data ***/

//Get VAM Inventory SKUs          
$vamItemArray = Array();          
$vaminventory = VAM_GetInventory();

if ($vaminventory[0]){
  while (!$vaminventory[1]->EOF && $row = $vaminventory[1]->GetRowAssoc(false)){               
              
    //Retrieve Item Info from VAM
    $row['stock'] = 'N/A';
    $stockInfo = VAM_GetWarehouseInventory($row['citemno'],'MAIN');
    if ($stockInfo[0]){ 
      $stockRow = $stockInfo[1]->GetRowAssoc(false);
      $row['stock'] = $stockRow['nonhand'];
    }
    
    $vamItemArray[$row['citemno']] = $row;
    
    $vaminventory[1]->MoveNext();
  }
}

include_once('Views/InventorySyncView.phtml');

?>