$(function() 
  { 

    /*
    ddsmoothmenu.init({
      mainmenuid: "smoothmenu1", //menu DIV id
      orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
      classname: 'ddsmoothmenu', //class added to menu's outer DIV
      //customtheme: ["#1c5a80", "#18374a"],
      contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
    })

    $("#loader").bind("ajaxSend", function() {
	        $(this).show();
	    }).bind("ajaxStop", function() {
	        $(this).hide();
	    }).bind("ajaxError", function() {
	        $(this).hide();
      }).bind("ajaxComplete", function() {
	        $(this).hide();
      });
  
    */
    //Bind focus and blur events for input colors
    $(':text:not(.noHighlight)').bind("focus", function(e){ $(this).addClass('selectedBox') } );
    $(':text:not(.noHighlight)').bind("blur", function(e){ $(this).removeClass('selectedBox') } );

    //Generate Date pickers for all date inputs
    //InitDatePicker('.dateInput');

    //Bind JS events to highlight Line-Item Rows on hover and click
    //HighlightItems();
    
    //Block all Enter presses and convert them to Tab if possible.
    //BlockEnter();

    //Trim all inputs for data-entry
    $('input:text[readonly!="readonly"]').each(function(){ 
      if ($(this).val() != '' && $(this).val() != undefined){
        $(this).val($.trim($(this).val())); 
      }
    });
    
    if ($('#warningDisplay').length != 0){
      $('#warningDisplay').show();
      $('#WarningDisplayButton').focus();
    }
    
  }
);

//Dirty Form Chcker
function InitDirtyForm(formID){

  $('#'+formID+' :input:not(.noDirty)').change(function(){
    if($(document).data('IsDirty') != 'dirty'){
      $(document).data('IsDirty','dirty');
    }
  });
  
  //Set the 
  window.onbeforeunload = DirtyCheck;
}

function DirtyCheck()
{
  if ($(document).data('IsDirty') == 'dirty'){
    return "You have not saved your changes.\nIf you continue you will lose all changes.";
  }

  return;
}

//Initialize stand Date Picker Widget
function InitDatePicker(elementName){

    //Bind Blur to Date inputs to pre-format 2-digit years with a 20 prefix
    $(elementName).blur(function(){ 
    
      var newYear = '';
      var thisVal = $(this).val();
      //If the value length is not undefined
      if (thisVal.length != undefined && !isNaN(thisVal.length)){
        var arrVals = $(this).val().split('/');
        //If there are 3 values upon the split
        if (arrVals.length == 3){
          var year = arrVals[2];
          //If the year is Not a number
          if (year.length == 4 && isNaN(year)){
            var prefix = year.substring(0,2);
            //But the first two digits are a number
            if (!isNaN(prefix)){
              var today = new Date();
              
              //prepend 20 before it for the new century
              newYear = String(today.getFullYear()).substring(0,2)+prefix;
              $(this).val(arrVals[0]+'/'+arrVals[1]+'/'+newYear);
            }
          }
        }
      }
    });
    
    //Generate Date pickers for all date inputs
    $(elementName).datepicker({  dateFormat: 'mm/dd/yy' ,
                                  showOn: "button",
                                  buttonImage: "js/calendar.gif",
                                  buttonImageOnly: true,
                                  onClose:function(dateText, inst) { 
                                            //Don't fire the validator when the calendar is closed
                                            //Makes usability easier.
                                            if ($('#'+inst.id).attr('data-bvalidator') != undefined){
                                              $('#'+inst.id).closest('form').data('bValidator').validate(false,$('#'+inst.id)); 
                                            }
                                          },
                                  beforeShow: function(input, inst) { $.data(document.body, 'validate','no') },
                                  onChangeMonthYear: function(year, month, inst) { $.data(document.body, 'validate','no') },
                                  //onSelect: function(dateText, inst) { $.data(document.body, 'validate','no') }
                                  showButtonPanel: true,
                                  changeMonth: true,
                                  changeYear: true
                              });

    //Add an Input-only Date Mask                          
    $(elementName).mask("99/99/9999");
    
}

//Form Validation Setup and Functions
var validationOptions = { validateOn: 'blur',
                          errorValidateOn: 'blur',
                          onBeforeAllValidations : function(jqObj) { 
                            if ($.data(document.body,'validate') == 'no') { 
                              $.data(document.body,'validate','yes'); 
                              return false; 
                            } 
                          },
                          onAfterValidate : function(jqObj, action, result){
                            if (!result){                              
                              var thisid = jqObj.parents('.ui-tabs-panel').attr('id');
                              $('a[href=#'+thisid+']').addClass('bvalidator_invalid');
                              jqObj.focus();                              
                            } 
                          },
                          onAfterAllValidations: function(element, result){                                    
                            if (!result){
                              $('#validationMsg').html("There is an error in the Form Data. <br/>Please Review all input fields before Saving");
                              $('#validationMsg').show(); 
                            } else {
                              $('#validationMsg').html("");
                              $('#validationMsg').hide(); 
                              $('#tabStrip a').removeClass('bvalidator_invalid');
                            }
                          }
                        };                 

function DisplayError(errString){

  $('#validationMsg').html(errString);
  $('#validationMsg').show();
  
}

function HideError(errString){

  $('#validationMsg').fadeOut();
  
}
  
//Global Behaviors, Events and Styles  
function HighlightItems(){
    $('.itemRow').hover(
      function (){ $(this).css('background-color','#DFFDFF'); },
      function (){ $(this).css('background-color','white'); }                   
    );    
    $('.itemRow').click(
      function(){ $(this).css('background-color','#3FFFC5'); }
    );
}

function HighlightTableRow(rowObject){
    $(rowObject).hover(
      function (){ $(this).css('background-color','#DFFDFF'); },
      function (){ $(this).css('background-color','white'); }                   
    );    
    $(rowObject).click(
      function(){ $(this).css('background-color','#3FFFC5'); }
    );
}

function GetDateForDisplay(inDate){

  var currentTime = new Date(inDate);

  if (currentTime.toString() == "NaN" || currentTime.toString() == "Invalid Date") {
    currentTime = new Date();
  }

  var currentTime = new Date();
  var month = String('00'+(currentTime.getMonth() + 1)).slice(-2);
  var day = String('00'+currentTime.getDate()).slice(-2);
  var year = currentTime.getFullYear();

  return month+'/'+day+'/'+year;
}
