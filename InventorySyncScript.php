<?php
ini_set('max_execution_time', 0); //300 seconds = 5 minutes
include_once('Functions/DBFunctions.php');
ConnectToVam();

include_once('Functions/VAMFunctions.php');
include_once('Functions/MailFunctions.php');
include_once('Functions/RESTFunctions.php');

include_once('Models/VAMInventoryClass.php');


$filters = Array(
  'search_critera' => Array(
    'fields' => 'sku'
  )
);
$syncResult = Array();


/** Prepare Inventory Export Data ***/
$vaminventory = VAM_GetWebInventory();					
$maginventory = GetMagentoRESTInventoryList();
if (!is_array($maginventory)) die('Web Item Retrieval Error: '.$maginventory);

$maginv = Array();
$limiter = 0;
/******UPDATE OR DISABLE *****/
foreach ($maginventory as $currentItem){
	
	$maginv[trim($currentItem['sku'])] = $currentItem;
	
	//IF the item exists in the selected vaminventory
	if (array_key_exists((string)trim($currentItem['sku']),$vaminventory)) {
	
		//IF the item was modified in the past 2 days then update it, otherwise move on.
		if (strtotime($vaminventory[trim($currentItem['sku'])]['tmodified']) >= strtotime('-2 days ',mktime()) ||
				strtotime($vaminventory[trim($currentItem['sku'])]['dcreate']) >= strtotime('-2 days ',mktime())
				){

			$inventoryObject = new RDI_VAMInventory($currentItem['sku']);
			$updateResult = $inventoryObject->UpdateMagento2Item($currentItem['sku'],'update');

			$syncResult[trim($currentItem['sku'])] = print_r($updateResult,true);
			LogResult(trim($currentItem['sku']),print_r($updateResult,true));
		}
	}	
  else {
		$vaminfo = VAM_GetItemDetails(trim($currentItem['sku']));
		if (!$vaminfo || (count($vaminfo) > 0 && trim(strtoupper($vaminfo['cstatus'])) != 'A') ){
			$updateInfo = Array('visibility' => 1, 'status' => 2);
			$deleteResult = UpdateMagento2Item(Array(trim($currentItem['sku']) => $updateInfo));
			if ($deleteResult[0]){
				if (intval($deleteResult[1][trim($currentItem['sku'])]) != 1){
					$syncResult[trim($currentItem['sku'])] = "ERROR: Could not Disable. ".print_r($deleteResult[1][trim($currentItem['sku'])],true);
					LogResult(trim($currentItem['sku']), "ERROR: Could not Disable. ".print_r($deleteResult[1][trim($currentItem['sku'])],true));
				} else{
				$syncResult[trim($currentItem['sku'])] = "SUCCESS: Item Disabled. ".(!$vaminfo?' WARNING: Item not found in AM':'');
				LogResult(trim($currentItem['sku']), "SUCCESS: Item Disabled".(!$vaminfo?' WARNING: Item not found in AM':''));
				}
			} else {
				$syncResult[trim($currentItem['sku'])] = "ERROR: Could not Disable. ".print_r($deleteResult[1],true);
				LogResult(trim($currentItem['sku']), "ERROR: Could not Disable. ".print_r($deleteResult[1],true));
			}
		}
	}
	$limiter++;
	if ($limiter > 1) break;
}
/*******************************************/

/****** CREATE ******/
$limiter = 0;
foreach ($vaminventory as $currentItemNo => $currentItemData){
	
	//IF the item number exists in the Magento inventory
	if (array_key_exists((string)trim($currentItemNo),$maginv)) {
		continue;
	}
	
	$inventoryObject = new RDI_VAMInventory($currentItemNo);
	$createResult = $inventoryObject->UpdateMagento2Item($currentItemNo, 'create');
	$syncResult[$currentItemNo] = print_r($createResult,true);
	//LogResult($currentItemNo,print_r($createResult,true));
  print_r($syncResult);
  die();
	break;
	//$limiter++;
	//if ($limiter > 0) break;
}

//SendMail('ksguleria@gmail.com','admin@smartdesks.com','SD Inventory Update -'.date('m/d/Y h:i:s'),print_r($syncResult,true));
print_r($syncResult);

return 1;

function LogResult($citemno, $message){

	$query = "insert into rdi_mag_syncs (citemno, dlog,cresult) values (?,?,?)";
	$parameters = Array($citemno,date('Y-m-d H:i:s'),$message);
	$result = ExecuteQuery($query,$parameters);
}
?>