<?php
function VAM_GetMagentoCustomerList(){

  $query = "select * from arcust where ecom_custid is not null order by dcreate desc";
  $result = ExecuteQuery($query);

  $custinfo = Array();
  if ($result[0] && $row = $result[1]->GetRowAssoc(false)){
    return $result[1];
  } 
  
  return false;

}

function VAM_GetMagentoCustomer($magid){

  $query = "select * from arcust where ecom_custid = ?";
  $parameters = Array($magid);
  $result = ExecuteQuery($query,$parameters);

  if ($result[0] && $result[1]->RecordCount() > 0 && $row = $result[1]->GetRowAssoc(false)){
    return $row;
  } 
    
  return false;

}

function VAM_GetMagentoOrders(){

  $query = "select * from arinvc where magento_id is not null order by cinvno desc";  
  $result = ExecuteQuery($query);
  
  return $result;
}

function VAM_GetMagentoSalesOrders(){

  $query = "select * from sosord where magento_id is not null". 
  //union select * from arinvch where magento_id is not null ".
  " order by dcreate desc";
  $result = ExecuteQuery($query);
  
  return $result;
}

function VAM_GetOrderByMagentoID($magid){

  $query = "select * from sosord where magento_id = ?";
  $parameters = Array($magid);
  $result = ExecuteQuery($query,$parameters);
  if ($result[0] && $result[1]->RecordCount() > 0 ){
    $row = $result[1]->GetRowAssoc(false);
    return  $row;
  }

  return false;
}

function VAM_GetInvoiceByMagentoID($magid){

  $query = "select * from arinvc where magento_id = ?";
  $parameters = Array($magid);
  $result = ExecuteQuery($query,$parameters);
  if ($result[0] && $result[1]->RecordCount() > 0 ){
    $row = $result[1]->GetRowAssoc(false);
    return  $row;
  }

  return false;
}

function VAM_SalespersonName($strSalesP){

  $query = "select cname from arslpn where cslpnno = ?";
  $parameters = Array($strSalesP);
  $result = ExecuteQuery($query,$parameters);
  if ($result[0] == true){
    return $result[1]->fields[0];
  }
  else {
    return '';
  }
}

function VAM_GLDescription($strID){

  global $conn;
$query = "exec [dbo].[rdicsp_GLAccountDescriptionSel] ?";
$parameters = Array($strID);
$result = sqlsrv_query($conn,$query,$parameters);

  if ($result && $row = sqlsrv_fetch_array($result)){
    return $row[0];
  }
  else {
    return '';  
  }

}

function VAM_CreateUID(){

  $retvalue = '';
  
  $query = "exec [dbo].[rdicsp_assignuidnew]";
  $result = ExecuteQuery($query);
  
  if ($result[0]){
    return $result[1]->fields[0];
  } else {
    return '';
		}
  
}

function VAM_CreateFullUID(){

  $retvalue = '';
  
  $query = "select newid()";
  $result = ExecuteQuery($query);
  
  if ($result[0]){
    return $result[1]->fields[0];
  } else {
    return '';
  }
  
}

function VAM_GetUID($conn, $csono,$citemno){

  $retvalue = '';
  
  $query = "select cuid from sostrs where csono = ? and citemno= ?";  
  $parameters = Array($csono, $citemno);
  $result = ExecuteQuery($query,$parameters);
  if ($result[0] && $row = $result->FetchRow()){   
    return $row[0];
  } else {
    return '';
  }
  
}

function VAM_GetInventory(){

  $query = "select * from icitem order by citemno asc";
  $result = ExecuteQuery($query);
  if ($result[0] ){
    return $result;
  }
  
  return Array();

}

function VAM_GetWebInventory(){

	$returnArray = Array();
	
  $query = "select * from icitem where cstatus = 'A' and lwebcart = 1  order by citemno asc";
  $result = ExecuteQuery($query);
  while ($result[0] && !$result[1]->EOF){
		$row = $result[1]->GetRowAssoc(false);
		$returnArray[trim($row['citemno'])] = $row;
    $result[1]->MoveNext();
  }
  
  return $returnArray;

}

function VAM_GetWooInventory(){

	$returnArray = Array();
	
  $query = "select * from icitem where lwebpublish = 1 order by citemno asc";
  $result = ExecuteQuery($query);
  while ($result[0] && !$result[1]->EOF){
		$row = $result[1]->GetRowAssoc(false);
		$returnArray[trim($row['citemno'])] = $row;
    $result[1]->MoveNext();
  }
  
  return $returnArray;

}

function VAM_GetWarehouseInventory($citemno, $cwarehouse = ''){

  if (trim($cwarehouse) == ''){
    $query = "select * from iciwhs where citemno = ? order by citemno asc";
    $parameters = Array($citemno);
  } else {
    $query = "select * from iciwhs where citemno = ? and cwarehouse = ? order by citemno asc";
    $parameters = Array($citemno, $cwarehouse);  
  }
  
  $result = ExecuteQuery($query,$parameters);
  if ($result[0]){
    return $result;
  }
  
  return Array();
  
}

function VAM_GetItemDetails($citemno){

  $query = "select icitem.*
            from icitem
            where icitem.citemno = ?";
  $parameters = Array($citemno);
  $result = ExecuteQuery($query,$parameters);

  if ($result[0] && $result[1]->RecordCount() > 0 && $row = $result[1]->GetRowAssoc(false)){
    return $row;
  }
	
	return Array();  
}

function VAM_GetItemCategory($citemno){

	$returnArray = array();
	
  $query = "select cwebcat
            from iciwcat
            where citemno = ?";
  $parameters = Array($citemno);
  $result = ExecuteQuery($query,$parameters);

  if ($result[0] && $result[1]->RecordCount() > 0){
		while (!$result[1]->EOF){
			$row = $result[1]->GetRowAssoc(false);
			$returnArray[] = intval($row['cwebcat']);
			$result[1]->MoveNext();
		}
  }
	
	return $returnArray;  
}

function VAM_GetItemSEO($citemno){

	$returnArray = array();
	
  $query = "select *
            from rdiseowords
            where citemno = ?";
  $parameters = Array($citemno);
  $result = ExecuteQuery($query,$parameters);
  if ($result[0] && $result[1]->RecordCount() > 0){
		$row = $result[1]->GetRowAssoc(false);
		$returnArray = $row;
	}
	
	return $returnArray;  
}

function VAM_GetItemShipRate($citemno){

  $query = "select nmisc
            from comisc,icitem
            where comisc.ctype = 'BUYER'
						and comisc.ccode = icitem.cbuyer
						and citemno = ?";
  $parameters = Array($citemno);
  $result = ExecuteQuery($query,$parameters);

  if ($result[0] && $result[1]->RecordCount() > 0 && $row = $result[1]->GetRowAssoc(false)){
    return floatval($row['nmisc']);
  }
	
	return 0;  
}
/*
function VAM_IncrementSystemSONumber($conn){

  $query = "update arsyst set csono = csono + 1";
  $result = ExecuteQuery($query);
      
  if (!$result[0]){  
    return "Error updating System SO #.<br/>".$result[1];
  } 
  
  return 'SUCCESS';
  
}*/

function VAM_GetCustNOFromSO($csono){

  $query = "select distinct ccustno from sosord where csono = ? union select distinct ccustno from sosordh where csono = ?";  
  $parameters = Array(str_pad($csono,10,' ',STR_PAD_LEFT),str_pad($csono,10,' ',STR_PAD_LEFT));
  $result = ExecuteQuery($query,$parameters);
    
  if ($result[0] && $row = $result[1]->FetchRow()){  
    return trim($row[0]);
  }
  
  return '';
  
}

function VAM_GetCustNOFromInvNo($cinvno){

  $query = "select distinct ccustno from arinvc where cinvno = ? union select distinct ccustno from arinvch where cinvno = ?";  
  $parameters = Array(str_pad($cinvno,10,' ',STR_PAD_LEFT),str_pad($cinvno,10,' ',STR_PAD_LEFT));
  $result = ExecuteQuery($query,$parameters);
    
  if ($result[0] && $row = $result[1]->FetchRow()){  
    return trim($row[0]);
  }
  
  return '';
  
}

function VAM_GetNextInvNumber() {
  
  $query = "select cinvno from arsyst";
  $result = ExecuteQuery($query);
  
  if ($result[0] && $row = $result[1]->FetchRow()){
    return $row[0];      
  }

  return '';
}

/*  
function VAM_IncrementSystemInvNumber(){

  $query = "update arsyst set cinvno = cinvno + 1";
  $result = ExecuteQuery($query);
      
  if (!$result[0]){  
    return "Error updating System SO #.<br/>".$result[1];
  } 

  return 'SUCCESS';
  
}*/

function VAM_GetCompanyDefaults() {
  
  $query = "select * from arsyst";
  $result = ExecuteQuery($query);
  
  if ($result[0] && $row = $result[1]->GetRowAssoc(false)){
    return $row;      
  }

  return '';
}

function VAM_GetPaycodeDetails($paycode){
  // Retrieve Paycode Info
  $query = "select npaytype,cdescript,cpaycode from arpycd where cpaycode = ?";
  $parameters = Array($paycode);
  $result = ExecuteQuery($query,$parameters);

  $arpycd = Array();
  if ($result[0] && $row = $result[1]->GetRowAssoc(false)){
    $arpycd = $row;
  } 
  
  return $arpycd;
}

function VAM_GetTaxcodeDetails($taxcode){
  // Retrieve Paycode Info
  $query = "select * from costax where lcurrent = 1 and ctaxcode = ?";
  $parameters = Array($taxcode);
  $result = ExecuteQuery($query,$parameters);

  $ctaxcode = Array();
  if ($result[0] && $row = $result[1]->GetRowAssoc(false)){
    $ctaxcode = $row;
  } 
  
  return $ctaxcode;
}

function VAM_GetCustomerInfo($ccustno){

  $query = "select * from arcust where ccustno = ? order by dcreate desc";
  $parameters = Array($ccustno);
  $result = ExecuteQuery($query,$parameters);

  $custinfo = Array();
  if ($result[0] && $row = $result[1]->GetRowAssoc(false)){
    $custinfo = $row;
  } 
  
  return $custinfo;

}

function VAM_EncryptCCNumber($ccnumber){

  $offsetArray = Array(32,31,30,29,28,27,26,25,24,33,32,31,30,29,28,27);
  
  $encryptedNumber = '';
  for ($i = 0; $i < strlen($ccnumber); $i++){
  
    $currentChar = '';
    $currentChar = substr($ccnumber,$i,1);
    
    $currentChar = ord($currentChar);
    
    $currentChar = intval($currentChar) + $offsetArray[$i];
    
    $encryptedNumber .= strtoupper(chr($currentChar));
  }
  
  return $encryptedNumber;
  
}

function VAM_DecryptCCNumber($ccnumber){

  $offsetArray = Array(32,31,30,29,28,27,26,25,24,33,32,31,30,29,28,27);
  
  $decryptedNumber = '';
  for ($i = 0; $i < strlen($ccnumber); $i++){
  
    $currentChar = '';
    $currentChar = substr($ccnumber,$i,1);
    
    $currentChar = ord($currentChar) - $offsetArray[$i];
    
    $currentChar = chr($currentChar);
        
    $decryptedNumber .= $currentChar;
  }
  
  return $decryptedNumber;
  
}