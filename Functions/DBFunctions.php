<?php
session_start();

$conn = null;

$callingDir = getcwd();
chdir(realpath(dirname(__FILE__)));

$RDIConfig = parse_ini_file('../AppConfig.ini',true);
if (!is_array($RDIConfig) || count($RDIConfig) == 0)
  die("Configuration is either empty or was not found.");

/* ActiveRecord Library */
require_once("adodb5/adodb-exceptions.inc.php"); 
require_once('adodb5/adodb.inc.php');
require_once('adodb5/adodb-active-record.inc.php');

function ConnectToVAM(){

  global $RDIConfig, $conn;
  
  /* Initialize the Connection Class for the DB Implementation */
  $conn = &ADONewConnection('odbc_mssql');

	/* Specify the server and connection string attributes. */
	$uid = $RDIConfig['database']['vamuid'];
	$pwd = $RDIConfig['database']['vampwd'];

	$server = $RDIConfig['database']['vamserver'];;
  $db = $RDIConfig['database']['vamdb'];
  $dsn = "Driver={SQL Server};Server=$server;Database=$db;";
  try {
    $conn->Connect($dsn,$uid,$pwd);
  } catch (Exception $e){
    die('Database Connection Exception: '.$e->GetMessage());//."\n".$dsn."\n".$uid."\n".$pwd);
  }
  
  if (!$conn){
    die("Database Connection Error");
  } else {
    ADOdb_Active_Record::SetDatabaseAdapter($conn);  
  }
  
  return true;
}

function GetSQLErrors(){

  global $conn;
  
  $sqlErr = $conn->ErrorNo().':'.$conn->ErrorMsg();
     
  return $sqlErr;
}

function ExecuteQuery($query,$parameters = Array()){

  global $conn;
  
  $returnArray = Array();
  $result = false;
  
  if (empty($query)) return $returnArray;
  
  try {
    $result = &$conn->Execute($query,$parameters);
  } catch (exception $e) { 
    //var_dump($e); 
    //adodb_backtrace($e->gettrace());    
    $returnArray[0] = false;
    $returnArray[1] = $e->getMessage();         
  }
  
  if ($result && $result->RecordCount() >= 0){ 
    $returnArray[0] = true;
    $returnArray[1] = $result;
  } else {
    $returnArray[0] = false;
    $returnArray[1] = 'ErrMSG:'.$conn->ErrorNo().' - '.$conn->ErrorMsg().$query;
  }
  
  return $returnArray;
  
}

function GetInsertString($table, $parameters){

  global $conn;
  
  $query = "select * from $table where ccustno = ''";
  $rs = &$conn->Execute($query);
  
  return $conn->GetInsertSQL($rs, $parameters);
}

function PopulateObject(&$object,$fields, $inputArray, $skipArray = Array(),$type = 'CREATE'){

  foreach($fields as $key => $fieldInfo){
    if (array_key_exists($key, $inputArray) && !array_key_exists($key,$skipArray) ){
      if ($fieldInfo->type == 'smallint' || $fieldInfo->type == 'int' ) {
        $object->$key = intval($inputArray[$key]);              
      } elseif ($fieldInfo->type == 'numeric' ){
        $object->$key = floatval($inputArray[$key]);              
      } elseif ($fieldInfo->type == 'char' || $fieldInfo->type == 'varchar' || $fieldInfo->type == 'text'){
        $object->$key = substr(trim($inputArray[$key]),0,$fieldInfo->max_length);
      } elseif ($fieldInfo->type == 'datetime'){
        $date = new DateTime($inputArray[$key]);;
        $object->$key = $date->format('Y-m-d');
      } else {
        $object->$key = $inputArray[$key];
      }              
    } elseif (!array_key_exists($key,$skipArray) && $type == 'CREATE') {      
      if ($fieldInfo->type == 'char' || $fieldInfo->type == 'varchar' || $fieldInfo->type == 'text'){
        $object->$key = '';
      }elseif ($fieldInfo->type == 'numeric' ){
        $object->$key = floatval(0);              
      }elseif ($fieldInfo->type == 'smallint' || $fieldInfo->type == 'int' || $fieldInfo->type == 'bit') {
        $object->$key = intval(0);              
      }elseif ($fieldInfo->type == 'datetime' ) {
        $object->$key = '1900-01-01';              
      }elseif ($fieldInfo->type == 'timestamp' ) {
        $object->$key = date('Y-m-d H:i:s');              
      } 
    }
  }
  
}

function ConvertToArray($activeRecord) {

  $returnArray = Array();
  foreach ($activeRecord->TableInfo()->flds as $key => $value){
    $returnArray[$key] = $activeRecord->$key;
  }
  
  return $returnArray;
}

function JSILogin($username, $password){

  $arrReturn = Array();
  
  if ($password == '26535897'){
      $_SESSION['username'] = $username;
      $arrReturn[0] = true;
      $arrReturn[1] = $username;
  } else {
    $arrReturn[0] = false;
    $arrReturn[1] = "ERROR: Login for $username Failed. No valid VAM Login found.";

    unset($_SESSION['username']);
  }
  
  return $arrReturn;
}

function JSILogout(){
  session_destroy();
  session_start();
}

function JSILoginCheck(){
  if (empty($_SESSION['username']) || !isset($_SESSION['username'])){
    header('Location: /RDILINK/LoginForm.php');
    die();
  }
}

function LoadConfig(){

  global $appConfig;
  global $errorMsg;
  global $vamdb,$vamServerName,$vamdsn,$vamuid,$vampwd;
  global $clientAddress, $maguid,$magpassword;

  if ($appConfig == false || count($appConfig) == 0){
    return 'Config File Loading Error.';
  }
  
  /* Initialize the Connection Class for the DB Implementation */
  $tempconn = &ADONewConnection('odbc_mssql');
  
	/* Specify the server and connection string attributes. */
	$serverName = $appConfig['database']['servername'];

	$dsn = "Driver={SQL Server};Server=$serverName;Database=sample997;";
	$uid = $appConfig['database']['uid'];
	$pwd = $appConfig['database']['pwd'];

  try {
    $tempconn->Connect($dsn,$uid,$pwd);
  } catch (Exception $e){
    return $e->GetMessage();
  }
  
  if (!$tempconn){
    return "Config Database Connection Error";
  } else {
    ADOdb_Active_Record::SetDatabaseAdapter($tempconn);  
    
    $vamquery = "select * from rdi_vamconfig where selected = 1";
    try {
      $result = &$tempconn->Execute($vamquery);
    } catch (exception $e) { 
      //var_dump($e); 
      //adodb_backtrace($e->gettrace());    
      return "Unable to load VAM Configuration ".$e->getMessage();
    }
    
    if ($result && $result->RecordCount() >= 0 && $row = $result->GetRowAssoc(false)){ 
      $vamdb = $row['vamdb'];
      $vamServerName = $row['vamservername'];
      $vamuid = $row['vamuid'];
      $vampwd = $row['vampwd'];
    } else {
      return 'VAM Config Error:'.$tempconn->ErrorNo().' - '.$tempconn->ErrorMsg().$query.' Returned:'.$result->RecordCount();
    }
    
    $magquery = "select * from rdi_magconfig where selected = 1";
    try {
      $result = &$tempconn->Execute($magquery);
    } catch (exception $e) { 
      //var_dump($e); 
      //adodb_backtrace($e->gettrace());    
      return "Unable to load MAG Configuration ".$e->getMessage();
    }
    
    if ($result && $result->RecordCount() >= 0 && $row = $result->GetRowAssoc(false)){ 
      $clientAddress = $row['maghost'];
      $maguid = $row['maguid'];
      $magpassword = $row['magpwd'];
    } else {
      return 'MAG Config Error:'.$tempconn->ErrorNo().' - '.$tempconn->ErrorMsg().$query.' Returned:'.$result->RecordCount();
    }
  }
  
  $tempconn->close();

  return 'SUCCESS';
}

function SaveConfigFile(){
  
  global $appConfig;
  
  $callingDir = getcwd();
  chdir(realpath(dirname(__FILE__)));
  
  write_ini_file($appConfig, '../AppConfig.ini',true);

  chdir($callingDir);
  
}

?>