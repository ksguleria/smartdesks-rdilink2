<?php

function RESTConnect(){

  global $RDIConfig;
  
  if (empty($RDIConfig['website']['magentourl'])){
    die('No REST URL provided for connection.');
  }
  
  if (empty($RDIConfig['website']['bearertoken'])){
    die('No Login Token provided for REST link.');
  }
  
  return true;
}

//makes GET,POST,PUT request based on type
function  REST_WebRequest($endPoint, $data = "", $type = "GET"){
	global $RDIConfig;
	
	//Set putting CBURL in consumer and changing that. In this way global CBURL is not modified
	$consumer = $RDIConfig['website']['magentourl'];
	$consumer .= $endPoint;
  //echo urldecode($consumer);
  
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$consumer);
  //dbg echo "$consumer \n";
	if(in_array($type, array('PUT', 'POST'))){			//request based option setting
		$data_json =	json_encode($data);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		
		//set header
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Authorization: Bearer ' . $RDIConfig['website']['bearertoken'],
			'Accept: application/json',
			'Content-Length: ' . strlen($data_json)
			)
		);
	}
  else if($type=="GET"){
	
		//set header auth
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Authorization: Bearer ' . $RDIConfig['website']['bearertoken'],
			'Accept: application/json')
		);
		
	}
	
	curl_setopt($ch, CURLOPT_TIMEOUT, 120); //timeout after 30 seconds  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
	
	$response = curl_exec($ch);
  if($response === false){
    return 'ERROR:' . curl_error($ch);
  }

	curl_close ($ch);

  $responseArr = json_decode($response,true);
  if ($responseArr == false){
    return "JSON ERROR: JSON data could not be parsed.\n $response";
  }
    
	// Error checking if message key is set
	if(array_key_exists('message',$responseArr)){
		return "RESPONSE ERROR: " . print_r($responseArr,true);
	}
	
	return $responseArr;
}

/** ORDER FUNCTIONS **/
function GetMagentoRESTOrderList($filter = Array()){
	$endPoint = "orders";
	
	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);	
	}
	
	$requestResponse = REST_WebRequest($endPoint . '?' . $query);
	
  if (!is_array($requestResponse)) {
		return "ERROR: Orders not found.".$requestResponse;

  }
  
  return $requestResponse['items'];
}

function GetMagentoRESTOrder($orderId = null, $filter = Array()){
	$endPoint = "orders/$orderId";
	
	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);	
	}
	
	return REST_WebRequest($endPoint . '?' . $query);
	
}

function GetMagentoRESTOrderById($orderId = null){
	$endPoint = "orders/$orderId";
	
  if (!is_numeric($orderId)) return "ERROR: Order ID is not a Number. $orderId";
  
  return REST_WebRequest($endPoint);
}

/** QUOTE FUNCTIONS **/
function GetMagentoRESTQuoteList($filter = Array()){
	$endPoint = "quotes";
	
	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);	
	}
	
	$requestResponse = REST_WebRequest($endPoint . '?' . $query);	
  if (!is_array($requestResponse)) {
		return "ERROR: Quotes not found. ".$requestResponse;
  }

  if (count($requestResponse['items']) == 1 && trim($requestResponse['items'][0]) == 'No Requests for Quote available.')
    return 'No Quotes found.';
  
  return $requestResponse['items'];
}

function GetMagentoRESTQuoteById($quoteId = null){
	$endPoint = "quotes/$quoteId";
	
  if (!is_numeric($quoteId)) return "ERROR: Quote ID is not a Number. $quoteId";
  
  return REST_WebRequest($endPoint);
}

function GetMagentoRESTQuoteItems($quoteId = null){
	$endPoint = "quotes/$quoteId/items/";
	
  if (!is_numeric($quoteId)) return "ERROR: Quote ID is not a Number. $quoteId";
  
  return REST_WebRequest($endPoint);
}

/** SHIPMENT FUNCTIONS **/

function GetMagentoRESTShipmentList($filter = Array()){
	$endPoint = "shipments";
	
	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);	
	}
	
	$requestResponse = REST_WebRequest($endPoint . '?' . $query);
	
  if (!is_array($requestResponse)) {
		return "ERROR: Shipments not found.".$requestResponse;

  }
  
  return $requestResponse['items'];
}

function GetMagentoRESTShipment($orderId = null, $filter = Array()){
	$endPoint = "shipment/$orderId";
	
	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);	
	}
	
	return REST_WebRequest($endPoint . '?' . $query);
	
}

function CreateMagentoRESTShipment($orders = array()){
			
  foreach ($orders as $orderid => $shipData){    
    
    $endPoint = "order/$orderid/ship";
    return REST_WebRequest($endPoint, $shipData, "POST");
  }
		
}


/** CUSTOMER FUNCTIONS **/
function GetMagentoRESTCustomer($id = null, $filter = array()){
	$endPoint = "customers/$id";

	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);
	}
	
	return REST_WebRequest($endPoint . '?' . $query);
}

function GetMagentoRESTCustomerById($id = null){
	$endPoint = "customers/$id";

  if (!is_numeric($id)) return "ERROR: Magento Customer ID is not a Number. $id";

  $query = http_build_query(array('searchCriteria'=>'ALL'));
	
	$requestResponse = REST_WebRequest($endPoint . '?' . $query);

  if ($requestResponse[0]){
    if (trim($requestResponse[1]['message'] != '')) return "ERROR - Magento Customer : ".$requestResponse[1]['message'];
  }

  return $requestResponse;
}

function GetMagentoRESTCustomerAddresses($id = null, $filter = array()){
	$endPoint = "customers/addresses/$id";

	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);
	}
	
	return REST_WebRequest($endPoint . '?' . $query);
}

function GetMagentoRESTCustomerAddressById($id = null){
	$endPoint = "customers/addresses/$id";

  $query = http_build_query(array('searchCriteria'=>'ALL'));
	
	$requestResponse = REST_WebRequest($endPoint . '?' . $query);

  if (!is_array($requestResponse)){
    return "ERROR - Magento Address : ".$requestResponse;
  } 
  
  return $requestResponse;
}


/** ATTRIBUTE FUNCTIONS **/
function GetMagentoRESTAttributeSetList($filter = array()){
	$endPoint = "eav/attribute-sets/list";
	
	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);	
	}
	
	return REST_WebRequest($endPoint . '?' . $query);
}

function GetMagentoRESTAttributeList($filter){
	$endPoint = "products/attribute-sets/sets/list";
	
	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);	
	}
	
	return REST_WebRequest($endPoint . '?' . $query);
	
}


/** INVENTORY FUNCTIONS **/
function GetMagentoRESTInventoryList($filter = Array()){
	$endPoint = "products";
	
	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);	
	}

	$requestResponse = REST_WebRequest($endPoint . '?' . $query);
  if (!is_array($requestResponse)) {
		return "ERROR: Items not found.\n".$requestResponse;
  }
  
  return $requestResponse['items'];
}

function GetMagentoRESTInventoryBySku($sku = '', $filter = array()){
	
	$endPoint = "products/$sku";
	
	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);	
	}
	return REST_WebRequest($endPoint . '?' . $query);
}

function GetMagentoRESTItemStockBySku($sku = '', $filter = array()){
	
	$endPoint = "stockItems/$sku";
	
	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);	
	}
	return REST_WebRequest($endPoint . '?' . $query);
}

function UpdateMagentoRESTItem($products = array()){
	
  foreach ($products as $sku => $productData){
	
		$endPoint = "products/$sku";
    return REST_WebRequest($endPoint, array("product"=>$productData), "PUT");
  }
		
}

function UpdateMagentoRESTItemStock($sku, $itemID, $productData = array()){
	
  $endPoint = "products/$sku/stockItems/$itemID";
  return REST_WebRequest($endPoint,$productData, "PUT");
		
}

function CreateMagentoRESTItem($products = array()){
	
  $endPoint = "products";
		
  foreach ($products as $sku => $productData){    
    return REST_WebRequest($endPoint, array("product"=>$productData), "POST");
  }
		
}

function GetMagentoRESTItemImages($amPath = null, $citemno = null){
	
	// if no image is set, then send empty array
	if($amPath == null || trim($amPath) == ''){
		return array();
	}
	
	// For base image check if file exists at this location
	$actualPath = str_replace("\\\GRUNT", "C:", $amPath);
	$actualPath = "C:\\xampp\\htdocs\\ACCTMATE-IMAGE\\WEBSTORE\\DETAIL\\2HL-6010-MP.jpg";
	
	if(!file_exists($actualPath)){
		// If file does not exist, return empty array
		return array();	
	}
	
	$baseImage = array(
		'position' => 0,
		'media_type' => 'image',
		'disabled' => false,
		'label' => '',
		'types' => array('thumbnail'),
		'content' => array(
				'type' => 'image/jpeg',
				'name' => pathinfo($actualPath,PATHINFO_FILENAME).'.'.pathinfo($actualPath, PATHINFO_EXTENSION),
				'base64_encoded_data' => base64_encode(file_get_contents($actualPath)),
		)
	);
	
	return array($baseImage);
}

/** CATEGORY FUNCTIONS **/
function GetMagentoRESTCategoryInfo($id = '', $filter = array()){
	if(trim($id) == "") return array(false, "Please send category ID with request.");
	
	$endPoint = "categories/$id";
	
	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);	
	}
	
	return REST_WebRequest($endPoint . '?' . $query);
	
}

function GetMagentoRESTCategoryList($filter = array()){
	
	$endPoint = "categories";
	
	if(count($filter) < 1){
		$query = http_build_query(array('searchCriteria'=>'ALL'));
	}else{
		$query = http_build_query($filter);	
	}
	
	return REST_WebRequest($endPoint . '?' . $query);
}

/** CUSTOMER FUNCTIONS **/

// create customer
function CreateMagentoRESTCustomer($magCust){
	$endPoint = "customers/me";
	return REST_WebRequest($endPoint, array("customer"=>$magCust), "PUT");
}

// update customer
function UpdateMagentoRESTCustomer($magCust){
	$endPoint = "customers/".$magCust["id"];
	return REST_WebRequest($endPoint, array("customer"=>$magCust), "PUT");
}

?>