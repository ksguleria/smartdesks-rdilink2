<?php

$connsole = null;

function ConnectToConsole(){

  global $RDIConfig, $connsole;
  
  /* Initialize the Connection Class for the DB Implementation */
  $connsole = &ADONewConnection('odbc_mssql');

	/* Specify the server and connection string attributes. */
	$uid = $RDIConfig['console']['consoleuid'];
	$pwd = $RDIConfig['console']['consolepwd'];

	$server = $RDIConfig['console']['consoleserver'];;
  $db = $RDIConfig['console']['consoledb'];
  $dsn = "Driver={SQL Server};Server=$server;Database=$db;";
  try {
    $connsole->Connect($dsn,$uid,$pwd);
  } catch (Exception $e){
    die("Console Connection Exception: ".$e->GetMessage());//."\n".$dsn."\n".$uid."\n".$pwd);
  }
  
  if (!$connsole){
    die("Console Connection Error");
  } else {
    ADOdb_Active_Record::SetDatabaseAdapter($connsole);  
  }
  
}

function ExecuteConsoleQuery($query,$parameters = Array()){

  global $connsole;
  
  $returnArray = Array();
  $result = false;
  
  if (empty($query)) return $returnArray;
  
  try {
    $result = &$connsole->Execute($query,$parameters);
  } catch (exception $e) { 
    $returnArray[0] = false;
    $returnArray[1] = $e->getMessage();         
  }
  
  if ($result && $result->RecordCount() >= 0){ 
    $returnArray[0] = true;
    $returnArray[1] = $result;
  } else {
    $returnArray[0] = false;
    $returnArray[1] = 'ErrMSG:'.$connsole->ErrorNo().' - '.$connsole->ErrorMsg().$query;
  }
  
  return $returnArray;
  
}

function GetConsoleInsertString($table, $parameters){

  global $connsole;
  
  $query = "select * from $table where ccustno = ''";
  $rs = &$connsole->Execute($query);
  
  return $connsole->GetInsertSQL($rs, $parameters);
}


?>