<?php
include_once('Functions/DBFunctions.php');
ConnectToVam();

include_once('Functions/ConsoleFunctions.php');
ConnectToConsole();

include_once('Functions/RESTFunctions.php');
RestConnect();

include_once('Functions/VAMFunctions.php');
include_once('Functions/MailFunctions.php');

require_once('Models/ConsoleQuoteClass.php');

$filter = Array(
            //'fields' => 'items[sku,extension_attributes[stock_item[qty]]]',
            'searchCriteria' => Array(
              //'pageSize' => 10,
              //'currentPage' => 1,
              'filter_groups' => Array(
                Array(
                  'filters' => Array(
                     Array(
                       'field' => 'status',
                       'value' => 'Processing',
                       'condition_type' => 'eq'
                     ),
                     Array(
                       'field' => 'status',
                       'value' => 'Pending',
                       'condition_type' => 'eq'
                     )
                   )
                ),
                Array(
                  'filters' => Array(
                     Array(
                       'field' => 'created_at',
                       'value' => date('Y-m-d',strtotime('-72 hours')),
                       'condition_type' => 'gteq'
                     )
                     //Array(
                     //  'field' => 'entity_id',
                     //  'value' => '40',
                     //  'condition_type' => 'eq'
                     //)
                  )
                )
              )
            )
          );
          
$syncResult = Array();

$orders = GetMagentoRESTOrderList($filter);
foreach ($orders as $currentOrder){        
  $quote = new RDI_ConsoleQuote();
  if ($quote->GetWebOrderID($currentOrder['entity_id']) > 0){
    echo 'Order Skipped. Entity ID '.$currentOrder['entity_id'].':'.$currentOrder['increment_id']."\n";
    continue;
  }

  $importResult = $quote->ImportWebOrder($currentOrder);
  if (!is_array($importResult)){
    $syncResult[] = 'Order # '.$currentOrder['increment_id'].' ('.$currentOrder['entity_id'].') : '.$importResult;
  } else {
    $syncResult[] = 'Order # '.$currentOrder['increment_id'].' ('.$currentOrder['entity_id'].')'.
      ' : Quote: Q'.$importResult['QuoteID'].
      ' Revision ID: '.$importResult['RevisionID'].
      ' Job ID: '.$importResult['JobID'];
  }
}


if (count($syncResult) > 0){
  //die(SendMail('mira@smartdesks.com', 'support@rdic.com','Order Download Report '.date('m/d/Y H:i:s'),implode("\n",$syncResult)));
  die(SendMail('allison@smartdesks.com', 'no-reply@smartdesks.com','Sandbox Order Download Report '.date('m/d/Y H:i:s'),implode("\r\n",$syncResult)));
}

?>