<?php

include_once('Functions/DBFunctions.php');
$conn = ConnectToVam();
include_once('Functions/RESTFunctions.php');
include_once('Functions/VAMFunctions.php');

include_once('Models/VAMSalesOrderClass.php');
include_once('Models/VAMInventoryClass.php');

/* POST processing*/
if ($_POST['ExportButton'] == 'Export Orders to VAM'){
  if (!SoapConnect()){
    $errorMsg = 'Error: failed to create session';  
  } else {
    $orderList = $_POST['magorders'];
    
    foreach($orderList as $magorder){
      $vamOrder = new RDI_VAMSalesOrder();    
      $syncResult = $vamOrder->ExportMagOrder($magorder).'<br/>';
      
      if (substr($syncResult,0,7) != 'SUCCESS') {
        $errorMsg .= $syncResult.'<br/>';
      } else {
        $successMsg .= substr($syncResult,7).'<br/>';
      }
    }
  }
}
else if ($_POST['UpdateInventoryButton'] == 'Update Magento Inventory'){
  if (!SoapConnect()){
    $errorMsg = 'Error: failed to create SOAP session';  
  } else {
    $itemList = $_POST['exportitems'];
    
    foreach($itemList as $vamitem){
      $itemObject = new RDI_VAMInventory($vamitem);    
      $syncResult = $itemObject->UpdateMagentoItem().'<br/>';
      
      if (substr($syncResult,0,7) != 'SUCCESS') {
        $errorMsg .= $syncResult.'<br/>';
      } else {
        $successMsg .= substr($syncResult,7).'<br/>';
      }
    }
  }
}

include_once('Views/SalesOrderSyncView.phtml');

?>