<?php
class arcust extends ADOdb_Active_Record{}
class arcnot extends ADOdb_Active_Record{}
class mktcccus extends ADOdb_Active_Record{}
class jsi_info extends ADOdb_Active_Record{}

class RDI_VAMCustomer {

  private $customer;
  private $notes;
  private $creditcards; 
  
	function RDI_VAMCustomer($ucid = ''){
  
    $this->customer = new arcust('arcust');
    $this->notes = new arcnot('arcnot');
    
    if (!empty($ucid)){
      $this->customer->load("ccustno=?",Array($ucid));
      $this->notes->load("ccustno=?",Array($ucid));
    }
	}
		
	public function SetCustomerNumber($ucid){
		$this->customer->ccustno = $ucid;
	}
	
  public function GetCustomer(){
    return $this->customer;
  }
  
	public function LoadCustomer(){
    
    $this->customer->load("ccustno=?",Array($this->customer->ccustno));
    return $this->customer;
        
	}
    
  public function GetNotes(){
    return $this->notes->mnotepad;    
  }

  public function GetCards(){
    return $this->creditcards->find("ccustno=?",Array($this->customer->ccustno));
  }
  
  public function GetCard($scrambledno){
    $this->creditcards->load("ccustno = ? and ccardno = ?", Array($this->customer->ccustno, EncryptValue(VAM_DecryptCCNumber(trim($scrambledno)), true) ) );    
    return $this->creditcards;    
  }
  
  public function GetPastDue(){
  
    
    $result = ExecuteQuery("exec [dbo].[rdicsp_PastDue] ?",Array($this->customer->ccustno));

    $pastDue = 0;
    if ($result[0] && $row = $result[1]->FetchRow()){
      $pastDue = $row[0];
    }

    return $pastDue;
      
  }    

  public function GetLastSaleDate(){
  
    global $conn;
    
    $query = "select max(dorder) as dorder from sosord where ccustno = ?";
    $parameters = Array($this->customer->ccustno);
    $result = ExecuteQuery($query,$parameters);

    $lastDate = 0;
    if ($result[0] && $row = $result[1]->GetRowAssoc(false)){
      $lastDate = $row['dorder'];
    }

    if ($lastDate == 0){
      return '';
    } else {
      return date('m/d/Y',strtotime($lastDate));
    }
      
  }    

  public function GetLastPaidDate(){
  
    global $conn;
    
    $query = "select max(dlastpaid) as dlastpaid from arinvc where dlastpaid is not null and ccustno = ?";
    $parameters = Array($this->customer->ccustno);
    $result = ExecuteQuery($query,$parameters);

    $lastDate = 0;
    if ($result[0] && $row = $result[1]->GetRowAssoc(false)){
      $lastDate = $row['dlastpaid'];
    }

    if ($lastDate == 0){
      return '';
    } else {
      return date('m/d/Y',strtotime($lastDate));
    }
      
  }    

  public function SaveCustomer($postVars){
  
    global $conn;
     
    //If no CCustno exists, then we're creating this customer anew    
    if (trim($this->customer->ccustno) == ''){

      $ucid = $postVars['ccustno'];
      if (empty($ucid)){
        return 'Error creating new customer.<br/>Please enter a Customer# to proceed with customer creation';
      }
      
      $exists = false;
      $query = "exec rdicsp_CheckCustomerNumber ?";
      $parameters = Array($ucid);
      $result = ExecuteQuery($query,$parameters);
      
      if ($result[0] == true && $row = $result[1]->GetRowAssoc(false)){        
        if (trim($row['idcount']) == 1){
          $exists = true;
        }
      }
      if ($exists) return 'Error creating new customer.<br/>'.$ucid.' already exists. Please select another Customer Number.';          

      //Retrieve Account ID in from Company Setup
      $query = "select * from arsyst";
      $result = ExecuteQuery($query);
      
      if ($result[0]){
        $acctInfo = $result[1]->GetRowAssoc(false);
      } else {
        return 'Error Creating new Customer.<br/>ARSYST Company Info not found';
      }
      
      //Fill up the Object with POSTed Variables
      PopulateObject($this->customer,$this->customer->TableInfo()->flds,$postVars,Array(),'CREATE');

      $this->customer->ccustno = strtoupper($ucid);    
      $this->customer->caracc = $acctInfo['caracc'];                 
      $this->customer->cwarehouse = $acctInfo['cwarehouse'];
      $this->customer->dcreate = date('Y-m-d');
      $this->customer->lgeninvc = $acctInfo['lgeninvc'];
      $this->customer->lsavecard = $acctInfo['lsavecard'];
      $this->customer->lapplytax = 1;
      $this->customer->ccurrcode = 'USD';
      
    } 
    else {
    
      //This is an update of an existing record so just copy over POSTed variables
      PopulateObjectNew($this->customer,$this->customer->TableInfo()->flds,$postVars,Array(),'UPDATE');
    }
    
    //Correct formats for Customer Fields
    if (!empty($this->customer->cfname)) $this->customer->cfname = ucfirst($this->customer->cfname);
    if (!empty($this->customer->clname)) $this->customer->clname = ucfirst($this->customer->clname);
      
    $this->customer->lsoemail = ($postVars['lsoemail'] == 1?1:0);
    
    $conn->StartTrans();
    try {
      $this->customer->save();
    } catch (exception $e) { 
      return $e->getMessage();         
    }
    
    //Save Notes
    $notesResult = $this->SaveNotes($postVars);
    //$infoResult = $this->SaveJSIInfo($postVars);

    $result = $conn->CompleteTrans();
    
    if (!$result){
      if ($notesResult != 'SUCCESS'){
        return 'Error Saving Customer<br/>'.$notesResult;
      } /*elseif ($infoResult != 'SUCCESS'){
        return 'Error Saving Customer<br/>'.$infoResult;
      }*/
    } else {
      return 'SUCCESS';      
    }
    
    return 'SUCCESS';      
     //SaveBillToShipTo
     
  }
    
  public function SaveNotes($postVars){
  
    if (empty($this->notes->ccustno)){
      if (!$this->notes->load("ccustno=?",Array($this->customer->ccustno))){
        $this->notes->ccustno = $this->customer->ccustno;
      }
    }
    
    $this->notes->mnotepad = trim($postVars['mnotepad']);    
    
    try {
      $this->notes->save();
    } catch (exception $e) { 
      return $e->getMessage();         
    }

    return 'SUCCESS';
  }
  
  /*
  public function GetJSIInfo(){
    return $this->jsi_info;
  }
 
 public function SaveJSIInfo($postVars){    

    $skipFields = Array();
    PopulateObject($this->jsi_info, $this->jsi_info->TableInfo()->flds, $postVars);
    $this->jsi_info->csono = $this->customer->ccustno;
    
    if ($postVars['zlpublist'] == 1){
      $this->jsi_info->zlpublist = 1;
    } else {
      $this->jsi_info->zlpublist = 0;
    }
    
    try {
      $this->jsi_info->save();
    } catch (Exception $e){
      return "Error saving Notes.<br/>".$e->getMessage();
    }
    
    return 'SUCCESS';
  }*/
   
  public function HasPastDueRentals(){

    $query = "select * from rnmast where ccustno = ? and dend < '".date('Y-m-d')."' and nstatus = 1";
    $parameters = Array($this->customer->ccustno);
    $result = ExecuteQuery($query,$parameters);

    if ($result[0] && $result[1]->RecordCount() > 0){
      return true;
    }

    return false;

  }

  public function HasOpenCredits(){

    $query = "select nopencr from arcust where ccustno = ?";
    $parameters = Array($this->customer->ccustno);
    $result = ExecuteQuery($query,$parameters);

    if ($result[0] && $result[1]->RecordCount() > 0 && $row = $result[1]->GetRowAssoc(false)){
      if ($row['nopencr'] > 0){
        return true;
      }
    }

    return false;
  }

  public function HasPastDueAmount(){

    $query = "select * from arinvc where ccustno = ? and nbalance > 0 and ddue < '".date('Y-m-d')."' ";
    $parameters = Array($this->customer->ccustno);
    $result = ExecuteQuery($query,$parameters);

    if ($result[0] && $result[1]->RecordCount() > 0){
      return true;
    }

    return false;

  }

  public function HasUninsuredRentals(){

    $query = "select * from rnmast where ccustno = ? and nstatus = 1 and lins = 0";
    $parameters = Array($this->customer->ccustno);
    $result = ExecuteQuery($query,$parameters);

    if ($result[0] && $result[1]->RecordCount() > 0){
      return true;
    }

    return false;

  }
  
  public function HasInsufficientDeposit(){
  
    $query = "select count(*) rentalCount from rnmast where nstatus = 1 and ccustno = ?";
    $parameters = Array($this->customer->ccustno);
    $result = ExecuteQuery($query,$parameters);

    $rentalCount = 0;
    if ($result[0] && $result[1]->RecordCount() > 0 && $row=$result[1]->GetRowAssoc(false)){
      $rentalCount = $row['rentalCount'];
    }
    
    $query = "select cexpdate from mktcccus where ccustno = ?";
    $parameters = Array($this->customer->ccustno);
    $result = ExecuteQuery($query,$parameters);

    $cardCount = 0;
    $validCount = 0;
    if ($result[0] && $result[1]->RecordCount() > 0){
       while( !$result[1]->EOF && $row = $result[1]->GetRowAssoc(false) ){

       $cardCount++;
        
        $expDate = $row['cexpdate'];
        list($month,$year) = explode('/',$expdate);

        $expiration = strtotime($month.'/01/'.'20'.$year);
        
        if (strtotime("now") < $expiration){
          $validCount++;
        }
        
        $result[1]->MoveNext();
      }
    }
  
    if ($validCount == 0 && $cardCount > 0 && $rentalCount > 0){
      return true;
    }
    
    return false;    
  }
  
  public function HasOpenOrder(){

    $query = "select nsoboamt from arcust where ccustno = ?";
    $parameters = Array($this->customer->ccustno);
    $result = ExecuteQuery($query,$parameters);

    if ($result[0] && $result[1]->RecordCount() > 0 && $row = $result[1]->GetRowAssoc(false)){
      if ($row['nsoboamt'] != 0){
        return true;
      }
    }

    return false;
  }

  public function HasOnHoldOrder(){

    $query = "select csono from sosord where ccustno = ? and lhold = 1";
    $parameters = Array($this->customer->ccustno);
    $result = ExecuteQuery($query,$parameters);

    if ($result[0] && $result[1]->RecordCount() > 0 ){
      return true;
    }

    return false;
  }

  public function HasDeclinedCard(){

    $query = "select csono from sosord where ccustno = ? and ldeclined = 1";
    $parameters = Array($this->customer->ccustno);
    $result = ExecuteQuery($query,$parameters);

    if ($result[0] && $result[1]->RecordCount() > 0 ){
      return true;
    }

    return false;
  }

  public function DeleteCustomer(){
    
    if (trim($this->customer->ccustno) == ''){
      return 'Delete ERROR: Customer ID not Loaded';
    }
    
    //Validations
    $historyQuery = "select cinvno from arinvc where ccustno = ?
                     union 
                     select cinvno from arinvch where ccustno = ?
                     union
                     select csono from sosord where ccustno = ?
                     union
                     select csono from sosordh where ccustno = ?
                     union
                     select crentalno from rnmast where ccustno = ?
                     union
                     select crentalno from rntran where ccustno = ?
                     ";
    $historyParameters = Array($this->customer->ccustno,
                               $this->customer->ccustno,
                               $this->customer->ccustno,
                               $this->customer->ccustno,
                               $this->customer->ccustno,
                               $this->customer->ccustno);
    $historyResult = ExecuteQuery($historyQuery, $historyParameters);
    
    if ($historyResult[0] && $historyResult[1]->RecordCount() > 0){
      return 'Delete ERROR: Customer cannot be deleted as they have transaction records in SO, INV or Rentals';    
    }
    
    try { 
      $deleteResult = $this->customer->delete();
    } catch (exception $e) { 
      return $e->getMessage();         
    }
    
    if (!$deleteResult){
      return $deleteResult;
    }
    
    return 'SUCCESS';
  }

  public function ExportCustomer($magorderdata){
  
    global $conn;
          
    if (trim($this->customer->ccustno) != ''){
      return 'Export Error: Customer Cannot be exported. Magento ID: '.$this->customer->ccustno;
    }
    
		//Generate a VAM Customer Code based on this order data
		$customerlastname = preg_replace("/[^a-zA-Z]+/", "", $magorderdata['customer_lastname']);
		$magcustomerid = strtoupper(substr($customerlastname,0,3)).substr(trim($magorderdata['billing_address']['postcode']),2,3);
		$exists = true;
		$counter = 1;
		$tempid = $magcustomerid;
		while ($counter <= 9999 && $exists == true){
			$query = "exec rdicsp_CheckCustomerNumber ?";
			$parameters = Array($tempid);
			$result = ExecuteQuery($query,$parameters);
			if ($result[0] == true && $row = $result[1]->GetRowAssoc(false)){  
				if (trim($row['idcount']) == 0){
					$exists = false;
					$magcustomerid = $tempid;
				} else {
					$tempid = $magcustomerid.$counter;
				}
			}
			$counter++;
		}

    $exists = false;
    $query = "exec rdicsp_CheckCustomerNumber ?";
    $parameters = Array($magcustomerid);
    $result = ExecuteQuery($query,$parameters);
    if ($result[0] == true && $row = $result[1]->GetRowAssoc(false)){        
      if (trim($row['idcount']) > 0){
        $exists = true;
      }
    } else {
			return 'Error creating new customer.<br/>'.$result[1];
	  }
		
    if ($exists) return 'Error creating new customer.<br/>'.$magcustomerid.' already exists.';

    //Retrieve Account ID in from Company Setup
    $query = "select * from arsyst";
    $result = ExecuteQuery($query);
    
    if ($result[0]){
      $acctInfo = $result[1]->GetRowAssoc(false);
    } else {
      return 'Error Creating new Customer.<br/>ARSYST Company Info not found';
    }
	
    //Retrieve Customer Address Info
    if ($magorderdata['customer_is_guest'] != 1){
      $magcustomer = GetMagentoCustomer($magorderdata['customer_id']);
      if (!is_array($magcustomer)) return $magcustomer;
      
      //Retrieve Customer Address Info
      $addFilter = Array(Array('key' => 'customer_id', 'value' => $magcustomer->customer_id), 
                         Array('key' => 'is_default_billing', 'value' => 1));
      $magaddressbook = GetMagentoCustomerAddressList($magcustomer['customer_id'], $addFilter);
      if (count($magaddressbook) == 0) {
        $magaddress = $magorderdata['billing_address'];    
      } else {
        $magaddress = $magaddressbook[0];
      }
    } else {
      $magaddress = $magorderdata['billing_address'];  
    }
    
    if (count($magaddress) == 0){
      return 'Customer Export Error: Could not retrieve address info for Magento Customer:';
    }
        
    //Assign retrieved values
    $dataArray['ecom_custid'] = $magorderdata['customer_id'];
    if ($magorderdata['customer_is_guest'] == 1){
      $dataArray['ecom_custid'] = -1;
    }
    $dataArray['ccustno'] = $magcustomerid;
    
		$dataArray['caddr1'] = $magaddress['street'];
		$dataArray['ccity'] = $magaddress['city'];
		$dataArray['cstate'] = $magaddress['region'];
		$dataArray['czip'] = $magaddress['postcode'];
		$dataArray['ccountry'] = $magaddress['country_id'];
		$dataArray['cphone1'] = $magaddress['telephone'];
		$dataArray['cphone2'] = '';
		$dataArray['cfax'] = $magaddress['fax'];
		$dataArray['cemail'] = $magcustomer['email'];
		$dataArray['cwebsite'] = '';

    if ($magorderdata['customer_is_guest'] == 1){
      $dataArray['ccompany'] = substr(ucfirst($magorderdata['customer_firstname']).' '.ucfirst($magorderdata['customer_lastname']),0);
      $dataArray['cemail'] = $magorderdata['customer_email'];
      $dataArray['cfname'] = ucfirst($magorderdata['customer_firstname']);
      $dataArray['clname'] = ucfirst($magorderdata['customer_lastname']);    
    } else {    
      $dataArray['ccompany'] = (trim($magaddress['company'] == '')?substr(ucfirst($magcustomer['firstname']).' '.ucfirst($magcustomer['lastname']),0):$magaddress['company']);
      $dataArray['cfname'] = ucfirst($magcustomer['firstname']);
      $dataArray['clname'] = ucfirst($magcustomer['lastname']);
    }
    
    //Values assigned per system settings
    $dataArray['caracc'] = $acctInfo['caracc'];                 
    $dataArray['cwarehouse'] = $acctInfo['cwarehouse'];
    $dataArray['dcreate'] = date('Y-m-d');
    $dataArray['lgeninvc'] = $acctInfo['lgeninvc'];
    $dataArray['lsavecard'] = $acctInfo['lsavecard'];
    $dataArray['ccurrcode'] = 'USD';      
    $dataArray['cstatus'] = 'A';
    
    //Fill up the Object 
    PopulateObject($this->customer,$this->customer->TableInfo()->flds,$dataArray,Array(),'CREATE');

    //print_r($this->customer);
    //die();
    $conn->StartTrans();
    try {
      $saveResult = $this->customer->save();
    } catch (exception $e) { 
      return $e->getMessage();         
    }
    
    //Save Notes
    //$notesResult = $this->SaveNotes($postVars);

    $result = $conn->CompleteTrans();
    
    if (!$result){
      if ($saveResult != 'SUCCESS'){
        return 'Error Saving Customer<br/>'.$saveResult;
      } 
    } 
    
    return 'SUCCESS'.trim($this->customer->ccustno);      
  }
  
}
?>