<?php
class icitem extends ADOdb_Active_Record{}
class iciwhs extends ADOdb_Active_Record{}
class icinot extends ADOdb_Active_Record{}
class icirmk extends ADOdb_Active_Record{}

class RDI_VAMInventory {

  private $icitem;
  private $iciwhs;
  private $icinot;
  private $icirmk;
	
	function RDI_VAMInventory($item = ''){
  
    $this->icitem = new icitem('icitem');
    $this->iciwhs = new iciwhs('iciwhs');
    $this->icinot = new icinot('icinot');
    $this->icirmk = new icirmk('icirmk');
    
    if (!empty($item)){
      $this->icitem->load("citemno=?",Array($item));
      $this->icinot->load("citemno=?",Array($item));
      $this->icirmk->load("citemno=?",Array($item));
    }
    }
			
  public function GetItem(){
    return $this->icitem;
  }
      
  public function GetNotes(){
    return $this->icinot->mnotepad;    
  }
  
  public function GetRemarks(){
    return $this->icirmk->mremark;    
  }

  public function GetWarehouse($warehouse){
  
    if (!empty($warehouse)){
     $this->iciwhs->load('cwarehouse = ? and citemno = ?',Array($warehouse,$this->icitem->citemno));
     return $this->iciwhs;
    } else {
      if ($rs = $this->iciwhs->find('citemno = ?',Array($this->icitem->citemno))){
        return $rs[0];
      }
    }
    
  }

  public function UpdateMagento2Item($itemno, $updateType){
		if (trim($updateType) == '' || ($updateType != 'create' && $updateType != 'update')) {
			return "ERROR: Update type not recognized";
		}

		$arrItemInfo = Array();
		$disabled = false;
    $itemInfo = Array();

    if (trim($this->icitem->citemno) == '' &&$updateType == 'create'){
      return "ERROR: Item # not found in VAM or not properly Loaded in PHP. ".$itemno;
		} 
		else {
    
      $itemInfo['attribute_set_id'] = '4';
      $itemInfo['sku'] = utf8_encode(trim($this->icitem->citemno));
      $itemInfo['name'] = utf8_encode(trim($this->icitem->cdescript));
      $itemInfo['weight'] = $this->icitem->nweight;
      $itemInfo['status'] = ($this->icitem->cstatus=='A'?1:2);
      $itemInfo['visibility'] = ($this->icitem->cstatus=='A'?'4':'1');
      $itemInfo['price'] = ($this->icitem->nspprice);

			//$priceQuery = "select * from icimlp where cpricecd = 'WEBSTORE' and citemno = ?";
			//$priceParameters = Array($this->icitem->citemno);
			//$priceResult = ExecuteQuery($priceQuery, $priceParameters);
			//if ($priceResult[0] && $priceResult[1]->RecordCount() > 0){
			//	$priceRow = $priceResult[1]->GetRowAssoc(false);			
			//} else {
      //  $priceRow = Array('nprice' => $this->icitem->nprice);
			//}
			
      //$itemInfo['price'] = $priceRow['nprice'];

      $itemInfo['custom_attributes'][]= Array('attribute_code'=>'description','value' => utf8_encode(trim($this->GetRemarks())));
      $itemInfo['custom_attributes'][]= Array('attribute_code'=>'short_description','value' => utf8_encode(trim($this->icitem->cdescript)));
      $itemInfo['custom_attributes'][]= Array('attribute_code'=>'tax_class_id','value' => ($this->icitem->ltaxable1==1?2:0));
      
      //$itemInfo['msrp'] = $this->icitem->nprice;    
      //$itemInfo['inventory_backorders'] = 1;
      //$itemInfo['am_shipping_peritem'] = VAM_GetItemShipRate($this->icitem->citemno);
				
        
      $seoWords = VAM_GetItemSEO($this->icitem->citemno);
      if (count($seoWords) > 0){
        $itemInfo['custom_attributes'][]= Array('attribute_code'=>'meta_description','value' => utf8_encode(trim($seoWords['keywords'])));
        
        $tags = $seoWords['tags'];
        
        $title = substr($tags, strpos(strtoupper($tags),'<TITLE>')+7,strpos(strtoupper($tags),'</TITLE>')-strpos(strtoupper($tags),'<TITLE>')-7);
        if (trim($title) != '') 
          $itemInfo['custom_attributes'][]= Array('attribute_code'=>'meta_title','value' => utf8_encode(trim($title)));
        
        if (strpos(strtoupper($tags),'<META NAME="KEYWORDS" CONTENT="') > 0){
          $keywords = substr($tags, strpos(strtoupper($tags),'<META NAME="KEYWORDS" CONTENT="')+31);//,strpos(strtoupper($tags),'<META NAME="DESCRIPTION"')-strpos(strtoupper($tags),'<META NAME="KEYWORDS" CONTENT="')-36);
          $keywords = substr($keywords, 0,strpos($keywords,'"'));
        }
        if (trim($keywords) != '') 
          $itemInfo['custom_attributes'][]= Array('attribute_code'=>'meta_keyword','value' => utf8_encode(trim($keywords)));
      }

			//$itemCategory = VAM_GetItemCategory($this->icitem->citemno);
			//if (count($itemCategory) > 0) {
			//	$itemInfo['categories'] = $itemCategory;
			//} else {
			//	return 'ERROR: Category Not found in AM';
			//}
      
      //Stock Data
      $stock = $this->GetWarehouse('MAIN');            
      $stockInfo['stock_item'] = Array();    
      $stockInfo['stock_item']['qty'] = $stock->nonhand;
      $stockInfo['stock_item']['is_in_stock'] = ($stock->nonhand > 0?true:false);                    
      $itemInfo['extension_attributes'] = $stockInfo;
      
      //Images
      $imageArray = $this->CreateProductImageArray($this->icitem->citemno, $this->icitem->cimagepath);
      $itemInfo['media_gallery_entries'] = Array($imageArray);

    }
			
    $arrItemInfo[$this->icitem->citemno] = $itemInfo;

    print_r($arrItemInfo);
    die($updateType);
		if ($updateType == 'update') {		
			$retval = UpdateMagentoRESTItem($arrItemInfo);
		} elseif ($updateType == 'create') {
			$retval = CreateMagentoRESTItem($arrItemInfo);
		} else {
			return 'ERROR: Update type not recognized: '.$updateType;
		}

		if (!is_array($retval)){
			return $retval;
		} else {
			
			$itemResult = $retval['id'];
			//$imageResult = $this->UploadProductImage();										
		}

		if ($disabled){
			return 'SUCCESS SKU HIDDEN';
		} else {
			return 'SUCCESS SKU '.trim($this->icitem->citemno).' '.$updateType.'d Successfully. ID:'.$itemResult."\n";//.' Image: '.$imageResult;
		}
	}	

	private function CreateProductImageArray($sku, $imagePath){

		$imageArray = Array();
			
		$allTypes = array('image','small_image','thumbnail','swatch_image');
		$noTypes = array();
		$allTypesUsed = false;
		
		if (file_exists($imagePath)){

			$imageData = @file_get_contents($imagePath);
				
			if ($imageData){
				$imageArray = array(
          'media_type' => 'image',
          'label' => trim($sku).'_image',
          'position' => 1,
          'disabled' => 0,
          'types'    => $allTypes,
          'content' => Array(
            'base64_encoded_data' => base64_encode($imageData),
            'type' => 'image/jpeg',
            'name' => substr($imagePath,strrpos($imagePath,'\\')+1)
          )
        );
			}
		}
		return $imageArray;	
  }
	
}

?>