<?php
class Quotes extends ADOdb_Active_Record{}
class QuoteRevisions extends ADOdb_Active_Record{}

$ClassDir = getcwd();
chdir(realpath(dirname(__FILE__)));


chdir($ClassDir);

class RDI_ConsoleQuote {

  private $quote;
  
	function RDI_ConsoleQuote($quoteid = ''){
  
    $this->quote = new Quotes('Quotes');
    if (!empty($quoteid))
      $this->LoadQuote($quoteid);
	}

  private function LoadQuote($quoteid){
    $this->quote->load('QuoteID = ?', Array(intval($quoteid)));  
  }
  
  public function GetWebOrderID($entityid){
    
    $query = "select QuoteID from Quotes where MagentoID = ?";
    $parameters = Array($entityid);    
    $result = ExecuteConsoleQuery($query,$parameters);
    if ($result[0] && $result[1]->RecordCount() > 0){
      $row = $result[1]->GetRowAssoc(false);
      return $row['quoteid'];
    } 

    return 0;
    
  }
  
  public function GetWebQuoteID($entityid){
    
    $query = "select QuoteID from Quotes where MagentoID = ?";
    $parameters = Array($entityid);    
    $result = ExecuteConsoleQuery($query,$parameters);
    if ($result[0] && $result[1]->RecordCount() > 0){
      $row = $result[1]->GetRowAssoc(false);
      return $row['quoteid'];
    } 

    return 0;
    
  }
  

  public function ImportWebOrder($weborder){

    global $RDIConfig;
    
    $response = $this->RequestConsole($RDIConfig['console']['quoterequest'],json_encode($weborder));
    list($quoteresult, $quoteid, $revisionid) = explode(':',$response);
    
    if (trim($quoteresult) != 'SUCCESS')
      return 'Quote Request failed: '.$response;

    $weborder['revisionid'] = $revisionid;
    $jobID = $this->RequestConsole($RDIConfig['console']['jobrequest'],json_encode($weborder));
    if (!is_numeric($jobID) || intval($jobID) == 0)
      return "Job Create ERROR for Quote $quoteid with Revision ID $revisionid. $jobID";
     
    return Array('QuoteID' => $quoteid, 'RevisionID' => $revisionid, 'JobID' => $jobID); 
  }
  
  public function ImportWebQuote($webquote){
    
    global $RDIConfig;
    
    $response = $this->RequestConsole($RDIConfig['console']['webquote'],json_encode($webquote));

    list($quoteresult, $quoteid, $revisionid) = explode(':',$response);    
    if (trim($quoteresult) != 'SUCCESS')
      return 'Quote Request failed: '.$response;
    
    return Array('QuoteID' => $quoteid, 'RevisionID' => $revisionid); 
  }

  private function RequestConsole($url, $postdata){

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Accept: application/json',
			'Content-Length: ' . strlen($postdata)
			)
		);
    
    curl_setopt($ch, CURLOPT_TIMEOUT, 120); //timeout after 30 seconds  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    
    $response = curl_exec($ch);
    if($response === false){
      return 'ERROR:'. curl_error($ch);
    }

    curl_close ($ch);
    
    return $response;
        
  }
}
?>