<?php
class sosord extends ADOdb_Active_Record{}
class sostrs extends ADOdb_Active_Record{}
class sosork extends ADOdb_Active_Record{}
class soivrk extends ADOdb_Active_Record{}
class sortrk extends ADOdb_Active_Record{}

$ClassDir = getcwd();
chdir(realpath(dirname(__FILE__)));

include_once('VAMCustomerClass.php');
require_once('../Functions/AUTHFunctions.php');
require_once('../Functions/MAGFunctions.php');

chdir($ClassDir);

class RDI_VAMSalesOrder {

  public $customerObject;
  public $sosord;
  private $sostrs;
  private $sosork;
  private $soivrk;
  
	function RDI_VAMSalesOrder($sono = '', $ucid = ''){
  
    $this->sosord = new sosord('sosord');
    $this->sostrs = new sostrs('sostrs');
    
    $this->sosork = new sosork('sosork');
    $this->soivrk = new soivrk('soivrk');
        
    if (!empty($sono)){
      $this->LoadOrder($sono);
    } elseif (!empty($ucid)){
      $this->LoadCustomer($ucid);
		}
	}

  private function LoadOrder($sono){
      $this->sosord->load('csono = ?', Array(str_pad($sono,10,' ',STR_PAD_LEFT)));  
      $this->LoadCustomer($this->sosord->ccustno);
      $this->LoadSORemarks();
      $this->LoadInvRemarks();
  }
  
  public function GetCustomer(){
    return $this->customerObject->GetCustomer();
  }
    
  public function GetCustomerObject(){
    return $this->customerObject;
  }
    
  private function LoadCustomer($ucid){
    $this->sosord->ccustno = $ucid;
    $this->customerObject = new RDI_VAMCustomer($ucid);        
  }
  
	public function SetOrderNumber($sono){
		LoadOrder($sono);
	}
	
	public function GetSalesOrder(){	
    return $this->sosord;
	}

	public function GetLineItems(){
    
	  return $this->sostrs->find('ccustno = ? and csono = ?',Array($this->sosord->ccustno, str_pad($this->sosord->csono,10,' ',STR_PAD_LEFT)));
	}

  public function LoadSORemarks(){  
    $this->sosork->load('csono = ?', Array(str_pad($this->sosord->csono,10,' ',STR_PAD_LEFT)));  
  }
  
  public function LoadInvRemarks(){  
    $this->soivrk->load('csono = ?',Array(str_pad($this->sosord->csono,10,' ',STR_PAD_LEFT)));
  }

  function GetSONumber() {
  
    $query = "select csono from arsyst";
    $result = ExecuteQuery($query);
    
    $soNumber = '';
    if ($result[0] && $row = $result[1]->FetchRow()){
      $soNumber = $row[0];      
    }

    return $soNumber;
  }
    
  function IncrementSystemSONumber(){

  $query = "update arsyst set csono = right('          '+rtrim(cast((csono + 1) as varchar(10))),10)";
  $result = ExecuteQuery($query);
      
  if (!$result[0]){  
    return "Error updating System SO #.<br/>".$result[1];
  } 
  
  return 'SUCCESS';
  
  }

  function GetSORemarks(){
    return $this->sosork;
  }

  function SaveSORemarks($remarks){
  
    $sosork = new sosork('sosork');    
    $sosork->load('csono = ?', Array(str_pad($this->sosord->csono,10,' ',STR_PAD_LEFT)));
    
    if (empty($sosork->csono)){
      $sosork->csono = $this->sosord->csono;
    }
    
    $sosork->msoremark = trim($remarks);    
    
    try {    
      $sosork->save();
    } catch (Exception $e){
      return 'Error saving SO Remarks<br/>'.$e->GetMessage();
    }
    
    return 'SUCCESS';
  }

  function GetInvRemarks(){
    return $this->soivrk;
  }

  function SaveInvRemarks(){
  
    $soivrk = new soivrk('soivrk');
    $soivrk->load('csono = ?',Array(str_pad($this->sosord->csono,10,' ',STR_PAD_LEFT)));
    
    if (empty($soivrk->csono)){
      $soivrk->csono = $this->sosord->csono;
    }
    
    $soivrk->mivremark = $_POST['INVRemarks'];
    
    try {
      $soivrk->save();
    } catch (Exception $e){
      return 'Error saving Invoice Remarks<br/>'.$e->getMessage();    
    }
    
    return 'SUCCESS';
  }

  function GetItemRemarks($cuid){
  
    $sortrk = new sortrk('sortrk');
    
    if ($sortrk->load('csono = ? and cuid= ? and ccustno = ?',Array($this->sosord->csono,$cuid, $this->sosord->ccustno))){
      return $sortrk->mremark;
    }
    
    return '';

  }

  function SaveItemRemarks($remarks, $cuid){
  
    $sortrk = new sortrk('sortrk');
    $sortrk->load('csono = ? and cuid= ? and ccustno = ?',Array($this->sosord->csono,$cuid, $this->sosord->ccustno));
    
    if (empty($sortrk->cuid)){
      $sortrk->csono = $this->sosord->csono;
      $sortrk->ccustno = $this->sosord->ccustno;
      $sortrk->cuid = $cuid;
    }
    
    $sortrk->mremark = trim($remarks);
    
    try {
      $sortrk->save();
    } catch (Exception $e){
      return 'Error saving Item Remarks<br/>'.$e->getMessage();    
    }
    
    return 'SUCCESS';
  }
  
  //Function designed to generate a memo for declined CC Verification
  //These notes are affixed the response field.
  private function GetDeclinedNotes($verification){
    $notes = '';
    
    $notes .= 'Order Details: '."\n";
    $notes .= 'Requested Order #: '.$this->sosord->csono."\n";
    $notes .= 'Transaction Date: '.date('m/d/Y')."\n";
    $notes .= 'Order Total: '.$verification->amount."\n";
    $notes .= 'Line Item Details:'."\n";
    
    $seqNos = $_POST['sequenceNos'];      
    foreach ($seqNos as $currentSeq){
      if ( $_POST['delitem_'.$currentSeq] != 'on'){
        $notes .= 'Item: '.$_POST['citemno_'.$currentSeq].' Price: '.$_POST['nprice_'.$currentSeq].'  Qty: '.$_POST['nordqty_'.$currentSeq]."\n";    
      }
    }
    
    return $notes;
  }
  
  function AdjustSOQty($citemno, $adjustmentQty, $adjustmentWarehouse){
  
    $query = "update iciwhs set nbook = CASE WHEN nbook+($adjustmentQty) > 0 THEN nbook+($adjustmentQty) ELSE 0 END where citemno = '$citemno' and cwarehouse = '$adjustmentWarehouse'";
    $result = ExecuteQuery($query);
    
    if (!$result[0]){
      return $result[1];
    }
    
    return 'SUCCESS';
  }
  
  function GetItemRevenueCode($citemno, $cwarehouse){
  
    $query = "Select crevncode from iciwhs where citemno = '$citemno' and cwarehouse = '$cwarehouse'";
    $result = ExecuteQuery($query);
    
    if ($result[0] && $result[1]->RecordCount() > 0 && $row = $result[1]->GetRowAssoc(false)){
      return $row['crevncode'];
    } 
    
    return '';
  }
  
  public function ExportMagOrder($magorderno){

    global $conn;
    
    /******** Initial Validations ***************/
    //Does order exist in VAM already?
    $query = "select csono from sosord where magento_id = ?";
    $parameters = Array(str_pad($magorderno,10,' ',STR_PAD_LEFT));
    $result = ExecuteQuery($query,$parameters);    
    if ($result[0] && $result[1]->RecordCount() > 0 && $row = $result[1]->GetRowAssoc(false)) {
      return "Import Error: Magento Order $magorderno already in VAM : CSONO = ".$row['csono'];
    }
    
    //Get Magento Order Data
    $magorderdata = GetMagentoOrder($magorderno);
    if (!is_array($magorderdata)){
      return "Import Error: ".$magorderdata;
    }

    //Generate a VAM SO Number
    $csono = $this->GetSONumber();
    if (empty($csono)){
      return "Import Error: Cannot get new VAM SO # for Order : $magorderno";
    }
    
    /*** Retrieve/Create Customer and Related Info ****/
    $customerid = '';
    //Get VAM Customer Data if he is not a guest by Magento Customer ID    
    if ($magorderdata['customer_is_guest'] != 1){
      $vamcustomer = VAM_GetMagentoCustomer($magorderdata['customer_id']);
      $customerid = $vamcustomer['ccustno'];    
    } else {
      $customeremail = trim($magorderdata['customer_email']);
      if ($customeremail == ''){
        return "Import Error: Anonymous Customer Email not available for Export.";
      }

      $vamemailquery = "select ccustno from arcust where cemail = ? order by dcreate asc";
      $vamemailparameters = Array($customeremail);
      $vamemailresult = ExecuteQuery($vamemailquery,$vamemailparameters);
      if ($vamemailresult[0] && $vamemailresult[1]->RecordCount() > 0){
        $vamcustomer = $vamemailresult[1]->GetRowAssoc(false);
        $customerid = $vamcustomer['ccustno'];    
      }
		}
    
    $this->customerObject = new RDI_VAMCustomer($customerid);    
    if (trim($this->customerObject->GetCustomer()->ccustno) == ''){
      $custResult = $this->customerObject->ExportCustomer($magorderdata);
      if (substr($custResult,0,7) != 'SUCCESS'){
        return 'Import Error: '.$custResult;
      } else {
        $this->customerObject = new RDI_VAMCustomer(substr($custResult,7));    
        if (trim($this->customerObject->GetCustomer()->ccustno) == ''){
          return 'Import Error: Unable to create VAM Customer with ID '.substr($custResult,7);
        } else {
          $warnings = '<br/>Customer '.substr($custResult,7).' Successfully created in VAM';
        }
      }
    }
    
    //Get Magento Tracking Numbers, if any
    $filter = array(array('order_id' => array ('=' => $magorderdata['order_id'])));        
    $shipmentArray = GetMagentoOrderShipmentList($filter);
    $trackingNumber = '';
    $shipDate = '';
    if (is_array($shipmentArray)){
      foreach ($shipmentArray as $currentShipment){
        $shipmentInfo = GetMagentoOrderShipmentInfo($currentShipment['increment_id']);
        if (!empty($shipmentInfo['tracks'][0]['track_number'])){
          $trackingNumber = $shipmentInfo['tracks'][0]['track_number'];
          $shipDate = date('m/d/Y',strtotime($shipmentInfo['tracks'][0]['created_at']));
        }
      }
    }
    
    $dataArray['magento_id'] = $magorderno;
    $dataArray['ccustno'] = $this->customerObject->GetCustomer()->ccustno;
    $dataArray['centerby'] = 'Vam Sync';
    $dataArray['cbcompany'] = $magorderdata['billing_address']['company'];
    $dataArray['cbaddr1'] = $magorderdata['billing_address']['street'];
    $dataArray['cbcity'] = $magorderdata['billing_address']['city'];
    $dataArray['cbstate'] = $magorderdata['billing_address']['region'];
    $dataArray['cbzip'] = $magorderdata['billing_address']['postcode'];
    $dataArray['cbcountry'] = $magorderdata['billing_address']['country_id'];
    $dataArray['cbphone'] = $magorderdata['billing_address']['telephone'];
    $dataArray['cbcontact'] = ucfirst($magorderdata['billing_address']['firstname']).' '.ucfirst($magorderdata['billing_address']['lastname']);
    $dataArray['cscompany'] = $magorderdata['shipping_address']['company'];
    $dataArray['csaddr1'] = $magorderdata['shipping_address']['street'];
    $dataArray['cscity'] = $magorderdata['shipping_address']['city'];
    $dataArray['csstate'] = $magorderdata['shipping_address']['region'];
    $dataArray['cszip'] = $magorderdata['shipping_address']['postcode'];
    $dataArray['cscountry'] = $magorderdata['shipping_address']['country_id'];
    $dataArray['csphone'] = $magorderdata['shipping_address']['telephone'];
    $dataArray['cscontact'] = ucfirst($magorderdata['shipping_address']['firstname']).' '.ucfirst($magorderdata['shipping_address']['lastname']);
    $dataArray['cshipvia'] = substr($magorderdata['shipping_method'],0,$fieldInfo['cshipvia']['max_length']);
    
    $dataArray['cpaycode'] = $magorderdata['payment']['method'];    
    $dataArray['cpono'] = $magorderdata['payment']['po_number'];
    $dataArray['cexpdate'] = $magorderdata['payment']['cc_exp_month'].'/'.$magorderdata['payment']['cc_exp_year'];
    $dataArray['ccardname'] = $magorderdata['payment']['cc_owner'];
    
    $dataArray['dorder'] = date('Y-m-d',strtotime($magorderdata['created_at']));
    $dataArray['ntaxable1'] = $magorderdata['base_subtotal'];
    $dataArray['nsalesamt'] = $magorderdata['subtotal'];
    $dataArray['ndiscamt'] = $magorderdata['discount_amount'];
    $dataArray['nfrtamt'] = $magorderdata['shipping_amount'];
    $dataArray['ntaxamt1'] = $magorderdata['tax_amount'];
    $dataArray['nweight'] = $magorderdata['weight'];
    $dataArray['nxchgrate'] = 1;
    $dataArray['ntaxver'] = 1;
    $dataArray['dcreate'] = date('Y-m-d');
    $dataArray['lapplytax'] = 1;
    $dataArray['csource'] = 'RDIMagSync';
    $dataArray['lquote'] = 0;

    //Assign customer values
    $dataArray['ccurrcode'] = 'USD';
    $dataArray['lusecusitm'] = intval($this->customerObject->GetCustomer()->lusecusitem);
    $dataArray['nxchgrate'] = 1;
    $dataArray['ndiscrate'] = 0;
    //$this->sosord->ndiscrate = $this->customerObject->GetCustomer()->ndiscrate;
    //$this->sosord->cslpnno = $this->customerObject->GetCustomer()->cslpnno;      

    //Fill in Empty values in the new record
    PopulateObject($this->sosord, $this->sosord->TableInfo()->flds, $dataArray, Array(),'CREATE');

    //Custom Value assignments for certain fields
    $this->sosord->csono = str_pad($csono,10,' ',STR_PAD_LEFT);
    $this->sosord->nftaxable1 = $this->sosord->ntaxable1;
    $this->sosord->nftaxable2 = $this->sosord->ntaxable2;
    $this->sosord->nfsalesamt = $this->sosord->nsalesamt;
    $this->sosord->nfdiscamt = $this->sosord->ndiscamt;
    $this->sosord->nffrtamt = $this->sosord->nfrtamt;
    $this->sosord->nftaxamt1 = $this->sosord->ntaxamt1;

    $conn->StartTrans();
    try {
      $saveResult = $this->sosord->save();
    } catch (Exception $e){
      return "Error Importing Sales Order<br/>".$e->getMessage();    
    }
        
    $incResult = $this->IncrementSystemSONumber();
    if ($incResult == 'SUCCESS'){
      $liResult = $this->ExportLineItems($magorderdata);
    } 
    
   if ($liResult != 'SUCCESS'){
        $conn->FailTrans();
    } else {
      $soRmkResult = $this->SaveSORemarks(trim($magorderdata['customerordercomment']));        

      if ($soRmkResult != 'SUCCESS') {
        $conn->FailTrans();
      }
    }
 
    $result = $conn->CompleteTrans();

    if (!$result){
      if ($incResult != 'SUCCESS'){
        return "Import Error: Could not increment VAM SO Number Counter for Magento Order $magorderno.<br/>".$incResult;
      } elseif ($liResult != 'SUCCESS'){
        return 'Error Exporting Line Items<br/>'.$liResult;
      } 
    } 
   
    return 'SUCCESS Order #'.$this->sosord->csono.' has been imported successfully.'.$warnings;
  }

  private function ExportLineItems($magorderdata){    

    $itemCounter = 1;
    $configurables = Array();
    foreach ($magorderdata['items'] as $magitem){
    
      if ($magitem['product_type'] == 'configurable') {
        $configurables[$magitem['sku']] = $magitem['sku'];
      } 
      elseif ($magitem['product_type'] == 'simple' && array_key_exists($magitem['sku'],$configurables)){
        continue;
      }
          
      $lineItem = new sostrs('sostrs');
      
      /***** Retrieve Item and related Info ****/
      $itemInfo = VAM_GetItemDetails($magitem['sku']);
      if (empty($itemInfo) || count($itemInfo) == 0){
        return 'Error creating line-item.<br/>Item Details not found in icitem: '.$magitem['sku'];
      }

      $cuid = VAM_CreateUID();
      if (empty($cuid)){
        return 'Error creating line-item.<br/>Unique ID could not be generated<br/>'.GetSQLErrors();
      }
             
     $warehouseInfo = VAM_GetWarehouseInventory($magitem['sku'],'MAIN');
      if (empty($warehouseInfo) || count($warehouseInfo) == 0){
        return 'Error creating line-item.<br/>MAIN Warehouse Details not found in ICIWHS: '.$magitem['sku'];
      }
     
      //Assign principle Item values
      $dataArray['cuid'] = $cuid;
      $dataArray['ccustno'] = $this->customerObject->GetCustomer()->ccustno;
      $dataArray['clineitem'] = str_pad($itemCounter, 5,'0',STR_PAD_LEFT);
      $dataArray['citemno'] = trim($magitem['sku']);
      $dataArray['cdescript'] = $magitem['name'];
      $dataArray['cwarehouse'] = 'MAIN';
      $dataArray['crevncode'] = $warehouseInfo['crevncode'];
      $dataArray['ctaxcode'] = '';
      $dataArray['drequest'] = date('Y-m-d',strtotime($magitem['created_at']));      
      $dataArray['ndiscrate'] = $magitem['discount_percent'];
      $dataArray['ntaxver'] = 1;
      $dataArray['nsalesamt'] = $magitem['base_price'];
      $dataArray['ndiscamt'] = $magitem['discount_amount'];      
      $dataArray['ntaxamt1'] = $magitem['tax_amount'];
      $dataArray['ntaxamt2'] = 0;
      $dataArray['ntaxamt3'] = 0;
      $dataArray['nordqty'] = $magitem['qty_ordered'];
      $dataArray['nshipqty'] = $magitem['qty_shipped'];
      $dataArray['nweight'] = $magitem['weight'];
      $dataArray['nprice'] = $magitem['price'];
      $dataArray['nseq'] = intval($itemCounter) * 10;
       //Assign item properties from icitem and iciwhs

      $lineItem->cmeasure = $itemInfo['cmeasure'];
      $lineItem->lkititem = $itemInfo['lkititem'];
      $lineItem->ltaxable1 = $itemInfo['ltaxable1'];
      $lineItem->ltaxable2 = $itemInfo['ltaxable2'];
      $lineItem->lowsormk = $itemInfo['lowsormk'];
      $lineItem->lptsormk = $itemInfo['lptsormk'];
      $lineItem->nqtydec = floatval($itemInfo['nqtydec']);
      $lineItem->nitmcnvqty = $itemInfo['ncnvqty'];
      $lineItem->ntrscnvqty = $itemInfo['ncnvqty'];
      $lineItem->ncost = $itemInfo['ncost'];

      //Fill in Object values
      PopulateObject($lineItem, $lineItem->TableInfo()->flds, $dataArray, Array(),'CREATE');

      $lineItem->nfsalesamt = $lineItem->nsalesamt;
      $lineItem->nfdiscamt = $lineItem->ndiscamt;
      $lineItem->nftaxamt1 = $lineItem->ntaxamt1;
      $lineItem->nftaxamt2 = $lineItem->ntaxamt2;
      $lineItem->nftaxamt3 = $lineItem->ntaxamt3;
      $lineItem->nfprice = $lineItem->nprice;

     //Custom formatted data fields
      $lineItem->csono = $this->sosord->csono;
      
      try {
        $lineItem->save();
      } catch (Exception $e){
        return 'Error Exporting Line-item ['.$lineItem->citemno.'].<br/> Could not insert new line-item record.<br/>'.$e->getMessage();
      }

      $remarkSave = $this->SaveItemRemarks(trim($magitem['itemcomment']),$cuid);

      if ($remarkSave != 'SUCCESS'){
        return ' Remarks could not be saved for Item #'.$lineItem->citemno.' '.$remarkSave;
      }
      
      $itemCounter++;
    }
          
    return 'SUCCESS';
  }
}
?>