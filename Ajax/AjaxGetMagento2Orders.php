<?php
include_once('../Functions/DBFunctions.php');
$conn = ConnectToVam();
include_once('../Functions/RESTFunctions.php');
include_once('../Functions/VAMFunctions.php');

// $type = $_POST['type'];
$type = 'SalesOrder';

$orders = GetMagentoRESTOrderList();
foreach ($orders as $currentOrder){        

  $isExported = false;
  $amOrder = '';
  if ($type == 'SalesOrder'){
    $amOrder = VAM_GetOrderByMagentoID($currentOrder['increment_id']);
    if ($amOrder) $isExported = true;    
  } elseif ($type == 'Invoice'){
    $amOrder = VAM_GetInvoiceByMagentoID($currentOrder['increment_id']);
    if ($amOrder) $isExported = true;
  } else {
    echo '<tr><td colspan="100%">ERROR Download type not recognized. '.$type.'</td></tr>';
    die();
  }

  $customercode = '<span style="color:blue">New</span>';
  if ($isExported) {
    $customercode = '<span style="color:green">'.$amOrder['ccustno'].'</span>';
  } else {
    if (isset($currentOrder['customer_id'])){
      $vamcustomer = VAM_GetMagentoCustomer($currentOrder['customer_id']);
      if ($vamcustomer)	$customercode = '<span style="color:green">'.$vamcustomer['ccustno'].'</span>';
    } else {
      $customercode = '<span style="color:blue">New Anon</span>';
    }
  }
  
  echo '<tr '.($isExported?'style="background-color:#CFF9E6"':'').'>'.'<td style="text-align:center">'.
  ($isExported?'AM SO '.trim($amOrder['csono']):'<input type=checkbox name="magorders[]" value="'.$currentOrder['increment_id'].'" class="importselect" />').'</td>'.
  '<td>'.$currentOrder['increment_id'].'</td>'.
  '<td>'.date('m/d/Y',strtotime($currentOrder['created_at'])).'</td>'.
  '<td>'.$customercode.'</td>'.
  '<td>'.$currentOrder['billing_address']['firstname'].' '.ucfirst($currentOrder['billing_address']['lastname']).'</td>'.
  '<td class="numeric">'.number_format($currentOrder['grand_total'],2).'</td>'.
  '<td>'.ucfirst($currentOrder['status']).'</td>'.
  '<td>'.$currentOrder['extension_attributes']['shipping_assignments'][0]['shipping']['address']['firstname'].'</td>'.
  '</tr>';
		
}    
?>