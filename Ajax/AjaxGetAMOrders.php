<?php
include_once('../Functions/DBFunctions.php');
$conn = ConnectToVam();

include_once('../Functions/VAMFunctions.php');

//include_once('../Models/VAMSalesOrderClass.php');

$vamOrders = VAM_GetMagentoSalesOrders();

while ($vamOrders[0] && !$vamOrders[1]->EOF && $currentOrder = $vamOrders[1]->GetRowAssoc(false)){

  $customer = VAM_GetCustomerInfo($currentOrder['ccustno']);

  echo '<tr>'.
    '<td>'.$currentOrder['csono'].'</td>'.
    '<td>'.$currentOrder['magento_id'].'</td>'.
    '<td>'.date('m/d/Y',strtotime($currentOrder['dorder'])).'</td>'.
    '<td>'.$currentOrder['ccustno'].'</td>'.
    '<td>'.$customer['cfname'].' '.$customer['clname'].'</td>'.
    '<td class="numeric">'.number_format($currentOrder['nsalesamt'] + $currentOrder['ntaxamt1'] + $currentOrder['nfrtamt'] - $currentOrder['ndiscamt'],2).'</td>'.
    '<td>'.$currentOrder['cbcompany'].'<br/>'.$currentOrder['cbcontact'].'<br/>'.$currentOrder['cbcity'].'<br/>'.$currentOrder['cbstate'].' '.$currentOrder['cbzip'].'</td>'.
    '<td>'.$currentOrder['cscompany'].'<br/>'.$currentOrder['cscontact'].'<br/>'.$currentOrder['cscity'].'<br/>'.$currentOrder['csstate'].' '.$currentOrder['cszip'].'</td>'.
    '</tr>';
    
 $vamOrders[1]->MoveNext();
}
?>