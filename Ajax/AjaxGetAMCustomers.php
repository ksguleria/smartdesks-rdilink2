<?php
include_once('../Functions/DBFunctions.php');
$conn = ConnectToVam();

include_once('../Functions/VAMFunctions.php');

$customers = VAM_GetMagentoCustomerList();

if ($customers){
  while (!$customers->EOF && $row = $customers->GetRowAssoc(false)){            
    echo '<tr>'.
      '<td>'.$row['ccustno'].'</td>'.
      '<td>'.$row['ecom_custid'].'</td>'.
      '<td>'.ucfirst($row['cfname']).' '.ucfirst($row['clname']).'</td>'.
      '<td>'.$row['cemail'].'</td>'.
      '<td>'.$row['ccity'].'</td>'.
      '<td>'.$row['cstate'].'</td>'.
      '<td>'.$row['ccountry'].'</td>'.
      '</tr>';
    $customers->MoveNext();
  }
}
          
?>