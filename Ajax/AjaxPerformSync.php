<?php
include_once('../Functions/DBFunctions.php');
$conn = ConnectToVam();
include_once('../Functions/RESTFunctions.php');
include_once('../Functions/MAGFunctions.php');
include_once('../Functions/VAMFunctions.php');
include_once('../Models/VAMInvoiceClass.php');
include_once('../Models/VAMSalesOrderClass.php');
include_once('../Models/VAMInventoryClass.php');

//PROCESS HTML POST
$itemList = $_POST['itemList'];
$type = $_POST['type'];
//END POST PROCESSING

if (count($itemList) == 0) {
  echo "ERROR: Item List is empty";
  return;
}

$ajaxResult = Array();
foreach ($itemList as $magorder){
  
  if ($type == 'SalesOrder'){
    $vamOrder = new RDI_VAMSalesOrder(); 
    $syncResult = $vamOrder->ExportMagOrder($magorder).'<br/>';
  } elseif ($type == 'Invoice'){
    $vamOrder = new RDI_VAMInvoice();    
    $syncResult = $vamOrder->ExportMagOrder($magorder).'<br/>';  
  } elseif ($type == 'Inventory'){
    $vamOrder = new RDI_VAMInventory($magorder);    
    $syncResult = $vamOrder->UpdateMagentoItem($magorder).'<br/>';
	} else if($type == 'WooInventory'){
		$vamOrder = new RDI_VAMInventory($magorder);    
    $syncResult = $vamOrder->UpdateMagento2Item($magorder).'<br/>';
  } else {
    echo "ERROR: Import Type not recognized. $type";
  }
  
  if (substr($syncResult,0,7) != 'SUCCESS') {
    $ajaxResult[$magorder] = $syncResult.'<br/>';
  } else {
    $ajaxResult[$magorder] = substr($syncResult,7).'<br/>';
  }
}

echo json_encode($ajaxResult);
return;

?>